# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Parallax::Application.config.secret_key_base = 'e74b9ebd974ebc45e144aa70a9e220c2c140d99b88922130a11f377de8d7e579f2e9380843f5a507f7fb3aeb8f628bf5a2b0a22e74105aba1ad27dc524120a86'
